var createArtNetObservable = require('artnet-observable')
var Insteon = require('home-controller').Insteon
var insteon = new Insteon()

var config = {
  18: {
    addr: '3de6c1',
    name: 'houseRight',
    state: 0
  },
  19: {
    addr: '3dad15',
    name: 'sconces',
    state: 0
  },
  17: {
    addr: '40d6a4',
    name: 'houseLeft',
    state: 0
  }
} 

var comName = '/dev/tty.usbserial-A60337V7'

var needToLink = process.argv[2] === 'link'
var isReady = !needToLink
var commandQueue = []
var commandProcessing = false

insteon.serial(comName, function () {
  console.log('Insteon USB connected')

  if (needToLink) {
    console.log('Linking...')

    var l = []
    for (c in config) l.push(config[c].addr)
    insteon.link(null, function (s) {
      console.log('done.')
      //isReady = true
    }) 
  }
})

  var keys = Object.keys(config)
  var keyIdx = 0;
  setInterval(function () {
    if (isReady && (1 ||!commandProcessing)) {
      keyIdx++
      if (keyIdx == keys.length) keyIdx = 0

      if (config[keys[keyIdx]].dirty) {
        var f = config[keys[keyIdx]]
        var c = [f.addr, f.state, 10, keys[keyIdx]]
        console.log('Processing: ', c)
        commandProcessing = c
        if(c[1] === 0) {
          config[c[3]].dirty = false
          insteon.light(c[0]).turnOff(c[2]).then(function (status) {
            console.log(status)
            if (status.response) {
              config[c[3]].dirty = false
            } else {
              console.log('failed', status)
            }
            //commandProcessing = false
          })
        } else {
          config[c[3]].dirty = false
          insteon.light(c[0]).turnOn(c[1],c[2]).then(function (status) {
            console.log(status)
            if (status.response) {
              config[c[3]].dirty = false
            } else {
              console.log('failed', status)
            }
            //commandProcessing = false
          })
        }
      }
    }
  }, 10)

  var tempConfig = {}
  var wCL
  const sub = createArtNetObservable().subscribe(function (package) {
    if (package.artnet && 'data' in package.artnet) {
      for (var channel in config) {
        var level = Math.floor((package.artnet.data[channel - 1]/255)*100)
        if (config[channel].state !== level) {
          console.log(channel, level)
          config[channel].state = level
          config[channel].dirty = true
        }
      }
    }
  })

